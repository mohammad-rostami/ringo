# Ringo

create your Ringtones from your musics simply or download them directly from our ringtone library... 

![ScreenShot](app/ringo.gif)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing

import it as a new project and you are good to go

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Mohammad Rostami** 

